#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	srand(time(0));
	int NumStr = -1, CountStr = 0;
	char **str;
	do
	{
		printf("Enter number of strings or 0 for entry to an empty string: ");
		scanf("%d", &NumStr);
	} while (NumStr<0);
	if (NumStr == 0)
	{
		NumStr = 10;
		str = (char**)malloc(NumStr * sizeof(char*));
		if (str == NULL)
		{
			puts("Not Enough Memory!");
			exit(1);
		}
		for (int i = 0; i < NumStr; i++)
		{
			if (i == NumStr - 1)
			{
				NumStr += 10;
				str = (char **)realloc(str, NumStr * sizeof(char*));
			}
			int NumCh = 10, CountCh = 0;
			str[i] = (char *)malloc(NumCh * sizeof(char));
			do
			{
				if (CountCh == NumCh - 1)
				{
					NumCh += 10;
					str[i] = (char *)realloc(str[i], NumCh * sizeof(char));
				}
				str[i][CountCh] = _getch();
				if ((str[i][CountCh] == '\b') && (CountCh>0))
				{
					str[i][CountCh] = 0;
					CountCh--;
					str[i][CountCh] = '\0';
					putchar('\b'); putchar('\0'); putchar('\b');
				}
				else
				{
					putchar(str[i][CountCh]);
					CountCh++;
				}
			} while (str[i][CountCh - 1] != 13);
			str[i][CountCh - 1] = '\0';
			printf("\n");
			if (strlen(str[i]) == 0)
				break;
			else
				CountStr++;
		}
	}
	else
	{
		CountStr = NumStr;
		str = (char**)malloc(NumStr * sizeof(char*));
		for (int i = 0; i < NumStr; i++)
		{
			int NumCh = 10, CountCh = 0;
			str[i] = (char *)malloc(NumCh * sizeof(char));
			do
			{
				if (CountCh == NumCh - 1)
				{
					NumCh += 10;
					str[i] = (char *)realloc(str[i], NumCh * sizeof(char));
				}
				str[i][CountCh] = _getch();
				if ((str[i][CountCh] == '\b') && (CountCh>0))
				{
					str[i][CountCh] = 0;
					CountCh--;
					str[i][CountCh] = '\0';
					putchar('\b'); putchar('\0'); putchar('\b');
				}
				else
				{
					putchar(str[i][CountCh]);
					CountCh++;
				}
			} while (str[i][CountCh - 1] != 13);
			str[i][CountCh - 1] = '\0';
			printf("\n");
		}
		printf("\n");
	}
	while (CountStr != 0)
	{
		int rnd = rand() % CountStr;
		printf("%s\n", str[rnd]);
		str[rnd] = str[CountStr - 1];
		CountStr--;
	}
	return 0;
}